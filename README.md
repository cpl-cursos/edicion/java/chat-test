Pasos seguidos.
===============

1 - Creado un nuevo proyecto maven en IntelliJ.

2 - Creado un archivo de config. desde https://start.spring.io/ con:
* websocket
* thymeleaf
* jpa (no necesario incialmente)
* spring-boot-devtools
* lombok (no necesario inicialmente)
* h2 database (no necesario inicialmente)
* spring-boot-starter-test

3 - Copiados los archivos generados en el paso anterior a la carpeta del proyecto sustutuyendo los generados por IntelliJ.

4 - Copiados los archivos del proyecto de demo (carpetas config, controller y model -renombrada a modelos-) al nuevo proyecto.

5 - Prueba de funcionamiento correcta.

6 - Paso del chat a la url "/salachat" con el archivo salachat.html (plantilla Thymeleaf). Index.html con un único enlace a "Sala de conversación".

7 - Se añaden archivos de fragmentos (thymeleaf) para <head> y <body> con los enlaces a los archivos de Bootstrap.

8 - Se convierte el cudro de nombre de usuario de la sala a elemento de Bootstrap para probar.

9 - Se modifican algunas propiedades de main.css ya que no funcionan bien con BS5.

ATENCION. Este proyecto no pretende proporcionar los estilos adecuados para su uso con BS, sino únicamente probar si BS funciona con el proyecto de web-socket original. NO SE HA REVISADO el archivo main.css para su optimización/mejora.

10 - Prueba de funcionamiento correcta.